package com.template_sdk.openlegacy;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import org.openlegacy.core.model.legacy.type.MainFrameLegacyTypes;
import org.openlegacy.core.annotations.common.*;
import org.openlegacy.core.model.field.RpcFieldType.*;
import org.openlegacy.core.annotations.rpc.Action;
import org.openlegacy.core.annotations.rpc.*;
import org.openlegacy.core.rpc.actions.RpcActions.EXECUTE;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@RpcEntity(name="Opnact31", language=Languages.COBOL)
@RpcActions(actions = {
        @Action(action = EXECUTE.class, path = "OPNACT31", displayName = "Execute", alias = "execute" )            })
public class Opnact31 implements org.openlegacy.core.rpc.RpcEntity {

    @RpcField(originalName = "DFHCOMMAREA", displayName = "Dfhcommarea")
    private Dfhcommarea dfhcommarea;

    @Getter
    @Setter
    @RpcPart(name = "Dfhcommarea", originalName = "DFHCOMMAREA", displayName = "Dfhcommarea")
    public static class Dfhcommarea {

        @RpcField(originalName = "CRT-ACCOUNT-IN", displayName = "CRTACCOUNTIN")
        private CrtAccountIn crtAccountIn;

        @RpcField(originalName = "ACCOUNT-OUT", displayName = "ACCOUNTOUT")
        private AccountOut accountOut;
    }

    @Getter
    @Setter
    @RpcPart(name = "CrtAccountIn", originalName = "CRT-ACCOUNT-IN", displayName = "CRTACCOUNTIN")
    public static class CrtAccountIn {

        @RpcField(length = 16, originalName = "ACTI-CUSTOMER-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiCustomerId;

        @RpcField(length = 16, originalName = "ACTI-CUSTOMER-NAME", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiCustomerName;

        @RpcField(length = 1, originalName = "ACTI-TYPCD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiTypcd;

        @RpcField(length = 3, originalName = "ACTI-SUB-TYPCD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiSubTypcd;

        @RpcField(length = 2, originalName = "ACTI-CNTRY-CD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiCntryCd;

        @RpcField(length = 4, originalName = "ACTI-BNK-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiBnkId;

        @RpcField(length = 6, originalName = "ACTI-BRNCH-ID", legacyType = MainFrameLegacyTypes.ZonedNumeric.class)
        @RpcNumericField(minimumValue = -999999, maximumValue = 999999, decimalPlaces = 0)
        private Integer actiBrnchId;

        @RpcField(length = 8, originalName = "ACTI-INITIAL-DEPOSIT", legacyType = MainFrameLegacyTypes.PackedDecimal.class, defaultValue = "0")
        @RpcNumericField(minimumValue = -99999999999.999D, maximumValue = 99999999999.999D, decimalPlaces = 3)
        private Double actiInitialDeposit;

        @RpcField(length = 3, originalName = "ACTI-CURRENCY", legacyType = MainFrameLegacyTypes.Char.class)
        private String actiCurrency;
    }

    @Getter
    @Setter
    @RpcPart(name = "AccountOut", originalName = "ACCOUNT-OUT", displayName = "ACCOUNTOUT")
    public static class AccountOut {

        @RpcField(originalName = "ACCOUNT-DETAILS", displayName = "ACCOUNTDETAILS")
        private AccountDetails accountDetails;

        @RpcField(length = 1, originalName = "RTCD", legacyType = MainFrameLegacyTypes.ZonedNumeric.class)
        @RpcNumericField(minimumValue = -9, maximumValue = 9, decimalPlaces = 0)
        private Integer rtcd;

        @RpcField(length = 60, originalName = "RT-MSG", legacyType = MainFrameLegacyTypes.Char.class)
        private String rtMsg;
    }

    @Getter
    @Setter
    @RpcPart(name = "AccountDetails", originalName = "ACCOUNT-DETAILS", displayName = "ACCOUNTDETAILS")
    public static class AccountDetails {

        @RpcField(length = 32, originalName = "ACTO-IBAN", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoIban;

        @RpcField(length = 11, originalName = "ACTO-ACCOUNT-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoAccountId;

        @RpcField(length = 2, originalName = "ACTO-CNTRY-CD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoCntryCd;

        @RpcField(length = 4, originalName = "ACTO-BNK-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoBnkId;

        @RpcField(length = 6, originalName = "ACTO-BRNCH-ID", legacyType = MainFrameLegacyTypes.ZonedNumeric.class)
        @RpcNumericField(minimumValue = -999999, maximumValue = 999999, decimalPlaces = 0)
        private Integer actoBrnchId;

        @RpcField(length = 16, originalName = "ACTO-CUSTOMER-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoCustomerId;

        @RpcField(length = 16, originalName = "ACTO-CUSTOMER-NAME", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoCustomerName;

        @RpcField(length = 1, originalName = "ACTO-TYPCD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoTypcd;

        @RpcField(length = 12, originalName = "ACTO-TYPE-NAME", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoTypeName;

        @RpcField(length = 3, originalName = "ACTO-SUB-TYPCD", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoSubTypcd;

        @RpcField(length = 40, originalName = "ACTO-TYPE-DESCRIPTION", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoTypeDescription;

        @RpcField(length = 8, originalName = "ACTO-BALANCE", legacyType = MainFrameLegacyTypes.PackedDecimal.class)
        @RpcNumericField(minimumValue = -99999999999.999D, maximumValue = 99999999999.999D, decimalPlaces = 3)
        private Double actoBalance;

        @RpcField(length = 3, originalName = "ACTO-CURRENCY", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoCurrency;

        @RpcField(length = 8, originalName = "ACTO-CRT-DT", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoCrtDt;

        @RpcField(length = 8, originalName = "ACTO-UPDT-DT", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoUpdtDt;

        @RpcField(length = 1, originalName = "ACTO-LOCKED", legacyType = MainFrameLegacyTypes.Char.class)
        private String actoLocked;
    }

}

