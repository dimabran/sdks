package com.template_sdk.openlegacy;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.List;
import java.util.Date;
import org.openlegacy.core.model.legacy.type.MainFrameLegacyTypes;
import org.openlegacy.core.annotations.common.*;
import org.openlegacy.core.model.field.RpcFieldType.*;
import org.openlegacy.core.annotations.rpc.Action;
import org.openlegacy.core.annotations.rpc.*;
import org.openlegacy.core.rpc.actions.RpcActions.EXECUTE;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@RpcEntity(name="Fininq2", language=Languages.COBOL)
@RpcActions(actions = {
        @Action(action = EXECUTE.class, path = "FININQ2", displayName = "Execute", alias = "execute" )            })
public class Fininq2 implements org.openlegacy.core.rpc.RpcEntity {

    @RpcField(originalName = "DFHCOMMAREA", displayName = "Dfhcommarea")
    private Dfhcommarea dfhcommarea;

    @Getter
    @Setter
    @RpcPart(name = "Dfhcommarea", originalName = "DFHCOMMAREA", displayName = "Dfhcommarea")
    public static class Dfhcommarea {

        @RpcField(length = 10, originalName = "CUST-ID", legacyType = MainFrameLegacyTypes.Char.class)
        private String custId;

        @RpcField(originalName = "CREDIT-CARDS", displayName = "CREDITCARDS")
        @RpcList(count = 5)
        private List<CreditCards> creditCards;
    }

    @Getter
    @Setter
    @RpcPart(name = "CreditCards", originalName = "CREDIT-CARDS", displayName = "CREDITCARDS")
    public static class CreditCards {

        @RpcField(length = 16, originalName = "CARD-NUMBER", legacyType = MainFrameLegacyTypes.Char.class)
        private String cardNumber;

        @RpcField(length = 16, originalName = "CARD-TYPE", legacyType = MainFrameLegacyTypes.Char.class)
        private String cardType;

        @RpcField(length = 2, originalName = "CARD-LIMIT", legacyType = MainFrameLegacyTypes.BinaryBigEndian.class)
        @RpcNumericField(minimumValue = -9999, maximumValue = 9999, decimalPlaces = 0)
        private Integer cardLimit;

        @RpcField(length = 2, originalName = "CARD-USAGE", legacyType = MainFrameLegacyTypes.BinaryBigEndian.class)
        @RpcNumericField(minimumValue = -9999, maximumValue = 9999, decimalPlaces = 0)
        private Integer cardUsage;
    }

}

